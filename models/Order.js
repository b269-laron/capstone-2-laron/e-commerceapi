const mongoose = require("mongoose");
const orderSchema = new mongoose.Schema({
	userId: {
		type: String,
		required: [true, "User ID is required"]
	},
	products: [
		{
			productId: {
				type: String,
				required: [true, "Product ID is required"]
			},

			quantity: {
				type: Number,
				required: [true, "Quantity is required"]
			},

			price: {
			type: Number
			},
			
			subTotal: {
				type: Number
			}
		}
	],
	totalAmount: {
		type: Number
	},
	purchasedOn: {
		type: Date,
		default: new Date(),
		timestamps: true
	},
	isActive : {
		type : Boolean,
		default: true
	}
});

module.exports = mongoose.model("Order", orderSchema);