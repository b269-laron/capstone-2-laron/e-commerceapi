const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const app = express();

const userRoute = require("./routes/userRoute");
const orderRoute = require("./routes/orderRoute");
const productRoute = require("./routes/productRoute");

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));

mongoose.connect( "mongodb+srv://laronjhon:admin123@zuitt-bootcamp.tyql6ow.mongodb.net/ECommerceAPI?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

app.use("/users", userRoute);
app.use("/products", productRoute);
app.use("/orders", orderRoute);

mongoose.connection.once("open", () => console.log(`Now connected to the cloud database`));
app.listen(process.env.PORT || 4000, console.log(`Now connected to port ${process.env.PORT || 4000}`))