const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	firstName : {
		type : String,
		required : [true, "First name is required"]
	},
	lastName : {
		type : String,
		required : [true, "Last name is required"]
	},
	email : {
		type : String,
		required : [true, "Email is required"]
	},
	password : {
		type : String,
		required : [true, "Password is required"]
	},
	isAdmin : {
		type : Boolean,
		default : false
	},
	mobileNo : {
		type : String, 
		required : [true, "Mobile No is required"]
	},

	order: [
			{
				productId : {
					type : String,
					required : [true, "Product ID is required"]
				},
				orderedOn : {
					type : Date,
					default : new Date()
				},
				isActive : {
					type : Boolean,
					default: true
				}
			}
		],

	// userCart: 
	// 	[
	// 		{
	// 			productId: {
	// 			type: String,
	// 			required: [true, "Product ID is required"]
	// 			},

	// 			quantity: {
	// 			type: Number,
	// 			required: [true, "Quantity is required"]
	// 			},

	// 			price: {
	// 			type: Number
	// 			},

	// 			subTotal: {
	// 			type: Number
	// 			},

	// 			totalPrice: {
	// 			type: Number
	// 			},
	// 			createdOn : {
	// 				type : Date,
	// 				default : new Date()
	// 			},
	// 		}

	// 	]
});

module.exports = mongoose.model("User", userSchema);