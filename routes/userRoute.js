const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");
const auth = require("../auth");

router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
});


router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});

// router.get("/:userId", (req, res) => {
// 	userController.userDetails(req.params).then(resultFromController => res.send(resultFromController));
// })



router.get("/details", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	userController.getProfile({id: userData.id}).then(resultFromController => res.send(resultFromController))
});


router.patch("/:userId/setAdmin", auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		};
	userController.setAdmin(data, req.params).then(resultFromController => res.send(resultFromController));
});

router.patch("/:userId/setUser", auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		};
	userController.setUser(data, req.params).then(resultFromController => res.send(resultFromController));
});


router.post("/archiveOrder", auth.verify, (req, res) =>{
	const data = {
		order: req.body
		};
	userController.archiveOrder(data).then(resultFromController => res.send(resultFromController));
});


router.get("/allUser", (req, res) => {
	userController.getAllUsers().then(resultFromController => res.send(resultFromController));
});

router.get("/:userId/userDetails", (req, res) => {
	userController.getUser(req.params).then(resultFromController => res.send(resultFromController));
});


router.put("/:userId/updateUser", auth.verify, (req, res) => {
	const data = {
		user: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		};
	userController.updateUser(data, req.params).then(resultFromController => res.send(resultFromController));
});


// router.post("/:userId/addToCart", auth.verify, (req, res) => {
// 	let data = {
// 		cart: req.body,
// 		}
// 	userController.AddToCart(data, req.params).then(resultFromController => res.send(resultFromController));
// });

router.post("/:userId/AddToCart", auth.verify, (req, res) => {
	let data = {
		cart: req.body
	}
	userController.addToCart(data, req.params).then(resultFromController => res.send(resultFromController));
});



module.exports = router;