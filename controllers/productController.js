const Product = require("../models/Product");

module.exports.addProduct = (data) => {
	if (data.isAdmin) {
		let newProduct = new Product({
			name: data.product.name,
	 		description: data.product.description,
	 		price: data.product.price,
	 		stocks: data.product.stocks,
	 		image: data.product.image
		});
		return newProduct.save().then((product, error) => {
			if (error) {
				return false;
			} else {
				return true;
			};
		});
	};

	let message = Promise.resolve('User must be an admin to add products on the database');
	return message.then((value) => {
		return {value};
	});
};

module.exports.getAllActiveProducts = () => {
	return Product.find({isActive:true}).then(result => {
		return result;
	});
};

// module.exports.getProduct = (reqParams) => {
// 	return Product.findById(reqParams.productId).then(result => {
// 		return ({
// 			"ID": result.id,
// 			"Name": result.name,
// 			"Description": result.description,
// 			"Price": result.price
// 		});
// 	});
// };

module.exports.updateProduct = (data, reqParams) => {
	if (data.isAdmin) {
		let newInfo = {
			name: data.product.name,
	 		description: data.product.description,
	 		price: data.product.price,
	 		stocks: data.product.stocks,
	 		image: data.product.image
		};
		return Product.findByIdAndUpdate(reqParams.productId, newInfo).then((product, error) => {
			if (error) {
				return false;
			} else {
				return true;
			};
		});
	};

	let message = Promise.resolve('User must be an admin to update product information on the database');
	return message.then((value) => {
		return {value};
	});
};

module.exports.archiveProduct = (data, reqParams) => {
	if (data.isAdmin) {
		let updateActiveField = {
			isActive: false
		};
		return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((product, error) => {
			if (error) {
				return false;
			} else {
				return true;
			};
		});
	};
};

module.exports.unarchiveProduct = (data, reqParams) => {
	if (data.isAdmin) {
		let updateActiveField = {
			isActive: true
		};
		return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((product, error) => {
			if (error) {
				return false;
			} else {
				return true;
			};
		});
	};
};



module.exports.getAllProducts = () => {
	return Product.find({}).then(result => {
		return result;
	});
};

// Retrieve specific course
module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		return result;
	});
};