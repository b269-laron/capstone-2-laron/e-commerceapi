const express = require("express");
const router = express.Router();
const orderController = require("../controllers/orderController");
const auth = require("../auth");

router.post("/createOrder", auth.verify, (req, res) => {
	let data = {
		userId: auth.decode(req.headers.authorization).id,
		order: req.body,
		}
	orderController.createOrder(data).then(resultFromController => res.send(resultFromController));
});


router.post("/updateOrderQuantity", auth.verify, (req, res) => {
	let data = {
		order: req.body
		}
	orderController.updateQuantity(data, req.params).then(resultFromController => res.send(resultFromController));
});

router.get("/activeOrders", (req, res) => {
	orderController.activeOrders().then(resultFromController => res.send(resultFromController));
});

router.patch("/archiveCustomerOrder", auth.verify, (req, res) => {
	const data = {
		order: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
	}
	orderController.archiveCustomerOrder(data).then(resultFromController => res.send(resultFromController));
});

router.get("/:orderId", auth.verify, (req, res) => {
	orderController.viewSingleOrder(req.params).then(resultFromController => res.send(resultFromController));
});

module.exports = router; 