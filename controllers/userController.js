const Order = require("../models/Order");
const Product = require("../models/Product");
const User = require("../models/User");

const bcrypt = require("bcrypt");
const auth = require("../auth");

// module.exports.registerUser = (reqBody) => {
// 	return User.find({email: reqBody.email}).then(result => {
// 		if (result.length > 0) {
// 			return "Email is unavailable!";
// 		} else {
// 			let newUser = new User({
// 				email: reqBody.email,
// 				password: bcrypt.hashSync(reqBody.password, 10)
// 			})			
// 			return newUser.save().then((user, error) => {
// 				if (error) {
// 					return false
// 				} else {
// 					return "Successful"
// 				}
// 			})
// 		};
// 	});
// };

module.exports.checkEmailExists = (reqBody) => {
	return User.find({email : reqBody.email}).then(result => {
		if (result.length > 0) {
			return true;
		} else {
			return false;
		};
	});
};


module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		password: bcrypt.hashSync(reqBody.password, 10)
	});
	return newUser.save().then((user, error) => {
		if (error) {
			return false;
		} else {
			return true;
		};
	});
};


module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result =>{
		if (result == null){
			return false;
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
			if (isPasswordCorrect) {
				return {access: auth.createAccessToken(result)}
			} else {
				return false;
			};
		};
	});
};


// module.exports.userDetails = (reqParams) => {
// 	return User.findById(reqParams.userId).then(result => {
// 		return ({
// 			"email": result.email,
// 			"password": result.password = ""		})	
// 	});
// }



module.exports.getProfile = (userData) => {
	return User.findById(userData.id).then(result => {
		if (result == null) {
			return false
		} else {
			result.password = "*****"
			// Returns the user information with the password as an empty string or asterisk.
			return result
		}
	})
};


module.exports.setAdmin = (data, reqParams) => {
if (data.isAdmin) {
		let updateActiveField = {
			isAdmin: true
		};
		return User.findByIdAndUpdate(reqParams.userId, updateActiveField).then((user, error) => {
			if (error) {
				return false;
			} else {
				return true;
			};
		});
	};
};


module.exports.setUser = (data, reqParams) => {
if (data.isAdmin) {
		let updateActiveField = {
			isAdmin: false
		};
		return User.findByIdAndUpdate(reqParams.userId, updateActiveField).then((user, error) => {
			if (error) {
				return false;
			} else {
				return true;
			};
		});
	};
};



module.exports.archiveOrder = (data) => {
	return User.findById(data.order.userId).then(result => {
		result.order.map(elem => { 
		  if(elem._id == data.order.cartId) {
		    elem.isActive = false
		 } result.save()
		});
		return "Order Removed!"
	});
};


module.exports.getAllUsers = () => {
	return User.find({}).then(result => {
		return result;
	});
};

module.exports.getUser = (reqParams) => {
	return User.findById(reqParams.userId).then(result => {
		return result;
	});
};


module.exports.updateUser = (data, reqParams) => {
			let newInfo = {
				firstName: data.user.firstName,
		 		lastName: data.user.lastName,
		 		email: data.user.email,
		 		mobileNo: data.user.mobileNo,
		 		password: data.user.password
		 		};

	return User.findByIdAndUpdate(reqParams.userId, newInfo).then((user, error) => {
			if (error) {
				return false;
			} else {
				return true;
			};
	})		


		let message = Promise.resolve('User must be an admin to update product information on the database');
		return message.then((value) => {
			return {value};
		});
};


