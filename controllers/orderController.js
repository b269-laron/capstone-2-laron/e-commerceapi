const Order = require("../models/Order");
const User = require("../models/User");
const Product = require("../models/Product");
const auth = require("../auth");


module.exports.createOrder = async (data) => {
	let userOrder = await User.findById(data.userId)
	.then(user => {
		if (user.admin) {
			return false  
		} else {
			data.order.products.map(elem => {
				user.order.push({productId: elem.productId})
			})
			user.save()
			return true
		}
	});

	if (userOrder.isAdmin) {
		return "Admin is not allowed to place an order!"
	} else {
		let productsInfo = []
		let priceInfo = 0
			for (var i = 0; i < data.order.products.length; i++) {
				productsInfo.push({
					productId: data.order.products[i].productId,
					quantity: data.order.products[i].quantity,
					price: await Product.findById(data.order.products[i].productId).exec()
						.then(productPrice => {
				 		return productPrice.price
						}),
					subTotal: await Product.findById(data.order.products[i].productId).exec()
						.then(productPrice => {
				 		return productPrice.price * data.order.products[i].quantity
						})
				})
			};		
			for (var i = 0; i < productsInfo.length; i++) {
						priceInfo+=productsInfo[i].subTotal
					}

		let newOrder = new Order({
			userId: data.userId,
			products: productsInfo,				
			totalAmount: priceInfo
		})
			return newOrder.save().then((user,error) => {
			if (error) {
				return false;
			} else {
				return true;
			}
		})
	} 
	return "Order Failed"
};




module.exports.updateQuantity = async (data) => {
	let price = await Product.findById(data.order.productId).exec()
			.then(product => {
				 return String(data.order.quantity * product.price)
			});

	let updatedPrice = await Order.findById(data.order.orderId).exec().then(result => {
		result.products.map(elem => { elem.quantity = data.order.quantity });
		result.totalAmount = price
		return result
	}).catch(error => {return false})

	return updatedPrice.save()
};

module.exports.activeOrders = () => {
	return Order.find({isActive:true}).then(result => {
		return result;
	});
};

module.exports.archiveCustomerOrder = (data) => {
	let updateActiveField = {
		isActive: false
	};
	return Order.findByIdAndUpdate(data.order.orderId, updateActiveField).then((order, error) => {
			if (error) {
				return false;
			} else {
				return "Order succesfully removed!";
			};
		});
	};

module.exports.viewSingleOrder = (reqParams) => {
	return Order.findById(reqParams.orderId).exec().then(result => {
		return result
	})
}


module.exports.archiveOrder = (reqParams) => {
	let deleteItem = Order.findById(reqParams.Id).then(result => {
		result.products.map(elem => {
			elem._id
		})
		return result
	})
}

// 	return Order.findById(reqParams.Id).then(result => {
// 		result.products.map(elem => {

// 		})

// 		})
// };