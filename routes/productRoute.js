const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController");
const auth = require("../auth");


router.post("/addProducts", auth.verify, (req, res) => {
	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
		};
	productController.addProduct(data).then(resultFromController => res.send(resultFromController));
});

router.get("/activeProduct", (req, res) => {
	productController.getAllActiveProducts().then(resultFromController => res.send(resultFromController));
});

// router.get("/:productId", (req, res) => {
// 	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));
// });

router.put("/:productId/updateProduct", auth.verify, (req, res) => {
	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		};
	productController.updateProduct(data, req.params).then(resultFromController => res.send(resultFromController));
});

router.patch("/:productId/archive", auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		};
	productController.archiveProduct(data, req.params).then(resultFromController => res.send(resultFromController));
});

router.patch("/:productId/unarchive", auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		};
	productController.unarchiveProduct(data, req.params).then(resultFromController => res.send(resultFromController));
});

router.get("/all", (req, res) => {
	productController.getAllProducts().then(resultFromController => res.send(resultFromController));
});

router.get("/:productId/productDetails", (req, res) => {
	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));
});



module.exports = router; 